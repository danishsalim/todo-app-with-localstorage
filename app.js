let todoInput = document.querySelector("#input-todo");
let todoList = document.querySelector(".todo-list");
let footer = document.querySelector("footer");
let activeButton = document.querySelector("#active");
let completedButton = document.querySelector("#completed");
let allButton = document.querySelector("#all");
let clearCompletedButton = document.querySelector("#clear");


let todoData = localStorage.getItem("items")
  ? JSON.parse(localStorage.getItem("items"))
  : [];
console.log(todoData);

todoInput.addEventListener("keydown", createTodo);
activeButton.addEventListener("click", showActiveTodo);
completedButton.addEventListener("click", showCompletedTodo);
allButton.addEventListener("click", showAllTodo);
clearCompletedButton.addEventListener("click", clearCompletedTodos);

function createId() {
  let date = new Date();
  let head = date.getTime();
  let tail = parseInt(Math.random(100) * 100);
  let id = head + tail;
  return id;
}

function createTodo(event) {
  if (event.key == "Enter") {
    let newTodoText = event.target.value.trim();
    if (newTodoText != "") {
      let todoObject = {
        id: createId(),
        text: newTodoText,
        status: "active",
      };
      todoData.push(todoObject);
      localStorage.setItem("items", JSON.stringify(todoData));
      location.reload();
    } else {
      return;
    }
  }
}

function displayTodo(data) {
  for (let index = 0; index < data.length; index++) {
    let li = document.createElement("li");
    li.className = "list-item-group";
    let checkbox = document.createElement("input");
    checkbox.type = "checkbox";
    checkbox.className = "todo-check";
    checkbox.id = data[index].id;
    li.appendChild(checkbox);
    let todoElement = document.createElement("input");
    todoElement.type = "text";
    todoElement.value = data[index].text;
    todoElement.className = "todo-description";
    todoElement.setAttribute("readonly", "readonly");
    li.appendChild(todoElement);
    let deleteButton = document.createElement("button");
    deleteButton.className = "delete";
    deleteButton.appendChild(document.createTextNode("X"));
    li.appendChild(deleteButton);
    todoList.appendChild(li);
    if (data[index].status == "completed") {
      checkbox.checked = true;
      todoElement.classList.add("completed");
    } else {
      checkbox.checked = false;
      todoElement.classList.remove("completed");
    }
  }
  todoInput.value = "";
  activateDeleteListeners();
  activateEditListeners();
  activateCheckboxListeners();
}

function activateDeleteListeners() {
  let delButtons = document.querySelectorAll(".delete");
  delButtons.forEach((delBtn, index) => {
    delBtn.addEventListener("click", () => deleteTodo(index));
  });
}

function deleteTodo(index) {
  todoData.splice(index, 1);
  localStorage.setItem("items", JSON.stringify(todoData));
  location.reload();
}

function activateEditListeners() {
  let inputElements = document.querySelectorAll(".todo-description");
  inputElements.forEach((input, index) => {
    input.addEventListener("dblclick", () => {
      input.removeAttribute("readonly");
      input.focus();
      let value = input.value;
      input.value = "";
      input.value = value;
    });
    input.addEventListener("focusout", () => {
      input.setAttribute("readonly", true);
    });
  });
}

function activateCheckboxListeners() {
  let allCheckboxes = document.querySelectorAll(".todo-check");
  allCheckboxes.forEach((checkbox, index) => {
    checkbox.addEventListener("change", () => {
      if (checkbox.checked) {
        for (let index = 0; index < todoData.length; index++) {
          if (todoData[index].id == checkbox.id) {
            todoData[index].status = "completed";
          }
        }
        showActiveTodo();
        localStorage.setItem("items", JSON.stringify(todoData));
        todoLeft()
      } else {
        for (let index = 0; index < todoData.length; index++) {
          if (todoData[index].id == checkbox.id) {
            todoData[index].status = "active";
          }
        }
        showCompletedTodo();
        localStorage.setItem("items", JSON.stringify(todoData));
        todoLeft()
      }
    });
  });
}

function showActiveTodo() {
  let activeTodos = todoData.filter((todo) => todo.status == "active");
  todoList.innerHTML = "";
  if (activeTodos.length) {
    displayTodo(activeTodos);
  }
}

function showCompletedTodo() {
  let completedTodos = todoData.filter((todo) => todo.status == "completed");
  todoList.innerHTML = "";
  if (completedTodos.length) {
    displayTodo(completedTodos);
    todoLeft()
  }
}

function showAllTodo() {
  location.reload()
}

function clearCompletedTodos() {
  let updatedData=todoData.filter((todo)=>{
    return todo.status!='completed'
  })
  localStorage.setItem("items", JSON.stringify(updatedData));
  location.reload();
}

function todoLeft() {
  let leftTodo = document.querySelector(".left-todo");
  let count=0
  todoData.forEach((todo)=>{
    if(todo.status=='active')
      {
        count++
      }
  })
  if (count <= 1) {
    leftTodo.textContent = count + " item left!";
  } else {
    leftTodo.textContent = count + " items left!";
  }
}


window.onload = () => {
  displayTodo(todoData);
  if (todoData.length) {
    footer.classList.remove("hide");
    todoLeft()
  }
};




